# WEB
Sistema de registro de asistentes a un evento específico 

DESARROLLADO POR: NAREN PERTUZ VIDES - INGENIERO DE SISTEMAS

NIVEL 1 Y 2
Versión de PHP: 8.0.2

INTRUCCIONES DE EJECUCIÓN

1. Luego de ejecutar los servidores en el Xampp proceda a crear la base de datos "Escuela " en PhpMyAdmin 
2. Importe el archivo SQL que se encuentra dentro de la carpeta llamada Database
3. Ingrese al Localhost desde el navegador y abra el archivo index.html que se encuentra en la carpeta raiz del proyecto
4. Una vez cargada dicha pagina ingrese usuario "admin" y contraseña 
"admin".

MOTOR DE BASE DE DATOS: MySQL 

Se utilizó el Framework Bootstrap,  así como también Javascript, Jquery y HTML.

IMAGENES DEL  SISTEMA DE REGISTRO EJECUTADO

INICIO DE SESIÓN
![Login](https://user-images.githubusercontent.com/80562232/111020307-eaa3cf80-8392-11eb-82e0-0cf8dda91af2.jpg)

VENTANA PRINCIPAL 
![Inicio](https://user-images.githubusercontent.com/80562232/111020364-44a49500-8393-11eb-9d54-9e91d19a8917.jpg)

FORMULARIO DE REGISTRO
![Registro](https://user-images.githubusercontent.com/80562232/111020365-453d2b80-8393-11eb-8736-9529ba914d20.jpg)

VENTANA DE ASISTENTES
![Asistentes](https://user-images.githubusercontent.com/80562232/111020363-440bfe80-8393-11eb-8e03-78ad9ac4a729.jpg)

NIVEL 3
https://sistemaasistentes.000webhostapp.com/index.html
